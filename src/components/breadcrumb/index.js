import React from 'react';
import Link from 'gatsby-link';
import { useLocation } from '@reach/router';

// Crumb creator
const createCrumb = (segment) => {
  return {
    path: segment,
    label: segment.replace(/-/g, ' ')
  }
}

const Breadcrumbs = () => {
  const location = useLocation();
  const segments = location.pathname.split('/').filter(Boolean);
  const segmentCount = segments.length;
  const crumbs = segments.map(item => createCrumb(item));

  // Split out seperator for easier reuse.
  const seperator = (
    <span className="mx-2" style={{ width: '12px' }}>
      <svg className="fill-current w-3 h-3 mx-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" /></svg>
    </span>
  );

  // If on homepage, return nothing
  if (location.pathname.indexOf('home') > -1 || location.pathname === '/') return false;

  // If we make it this far, we are on a subpage
  return (
    <div className="my-8 mb-0 bg-white">
      <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
        <nav className="text-black font-medium py-0 capitalize" aria-label="Breadcrumb">
          <ol className="list-none p-0 inline-flex">
            <li className="flex items-center " key="breadcrumb-root"><Link to="/">Home</Link></li>
            {crumbs && crumbs.map((crumb, index) => {
              // If the last item, we dont want a link
              if (index + 1 === segmentCount) return (
                <li className="flex items-center text-gray-500" key={`crumb-${index}`} aria-current="page">{seperator}{crumb.label}</li>
              )

              // Standard crumb
              return (
                <li className="flex items-center" key="breadcrumb-current">
                  {seperator}
                  <Link to={`/` + crumb.path}>{crumb.label}</Link>
                </li>)
            })}
          </ol>
        </nav>
      </div>
    </div>
  )
};

export default Breadcrumbs;
