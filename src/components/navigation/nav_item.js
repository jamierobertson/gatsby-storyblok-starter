import React from 'react';
import Link from 'gatsby-link';

const NavItem = ({ blok }) => {
  // Workaround for homepage slug bug.
  if (blok.link.cached_url === 'home') blok.link.cached_url = '';

  return (
    <li className="nav-item mb-0">
      <Link
        to={`/${blok.link.cached_url}`}
        activeClassName="text-indigo-600 font-semibold"
        partiallyActive={blok.link.cached_url === '' ? false : true}
        className="text-base leading-6 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150"
        role="menuitem">
        {blok.title}
      </Link>
    </li>
  )
};

export default NavItem;
