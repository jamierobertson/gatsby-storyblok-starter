import React from 'react';
import Link from 'gatsby-link';
import NavItem from '@components/navigation/nav_item';

const Nav = ({ settings }) => {
  return (
    <div className="relative bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6">
        <div className="flex justify-between items-center border-b-2 border-gray-200 py-6 md:justify-start md:space-x-10">
          <div className="lg:w-0 lg:flex-1">
            <Link to="/" className="flex">
              <img className="h-8 w-auto sm:h-10 mb-0" width="44px" src="https://tailwindui.com/img/logos/v1/workflow-mark-on-white.svg" alt="Workflow" />
            </Link>
          </div>
          <nav>
            <ul className="hidden md:flex space-x-10">
              {settings &&
                settings.content.main_navi.map((navitem, index) => <NavItem blok={navitem} key={index} />)
              }
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}

export default Nav;
