import React from 'react';
import { render } from 'storyblok-rich-text-react-renderer';
import Image from '@components/image';

const AuthorCard = ({ blok }) => {

  if (!blok.content) return false;

  return (
    <div className="py-15 bg-white">
      <div className="p-5 pt-20 pb-20 border-t-2 border-gray-200">
        <div className="sm:flex items-center max-w-4xl">
          <Image
            className="w-9/12 rounded-full sm:mr-10"
            image={blok.content.profile_pic}
            alt={`Avatar of ${blok.content.name}`}
            simpleFluid
          />
          <div className="text-left">
            <div className="mb-4 text-gray-500">
              <h2 className="text-base leading-6 text-indigo-600 font-semibold tracking-wide uppercase">About the author</h2>
              <div className="mt-2 text-base leading-6">{render(blok.content.bio)}</div>
              <div className="text-md mt-5">
                <h3 className="font-medium leading-none text-gray-900 hover:text-indigo-600 transition duration-500 ease-in-out">{blok.content.name}</h3>
                <p>{blok.content.role}</p>
                <a href={`mailto:${blok.content.email.email}`} className="text-indigo-600">Get in touch</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  )
};

export default AuthorCard;



