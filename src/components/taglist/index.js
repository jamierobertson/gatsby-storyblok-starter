import React from 'react';
import Link from 'gatsby-link';

const TagList = props => {
  const tags = Object.values(props);

  if (!tags.length) return false;

  return (
    <div className="bg-white">
      <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
        <h3 className="mb-3 text-gray-500">Tags:</h3>
        <div className="md:flex mb-10 text-gray-500">
          {tags && tags.map(tag =>
            <Link to={`/tags/${tag}`} key={tag}>
              <span className="text-xs inline-flex items-center font-bold leading-sm uppercase px-3 py-1 mr-3 bg-blue-200 text-blue-700 rounded-full" key={tag}>
                {tag}
              </span>
            </Link>
          )}
        </div>
      </div>
    </div>
  )
};

export default TagList;
