import React from 'react';
import Link from 'gatsby-link';
import CreateMarkup from '@utils/createMarkup';
import SbEditable from 'storyblok-react';
import Image from '@components/image';

const Hero = props => {
  const intro = CreateMarkup(props.blok.introduction);

  return (
    <SbEditable content={props.blok} key={props.blok._uid}>
      <div className="relative bg-white overflow-hidden">
        <div className="max-w-screen-xl mx-auto">
          <div className="relative z-10 pb-8 bg-white sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-28 xl:pb-32">

            <svg className="hidden lg:block absolute right-0 inset-y-0 h-full w-48 text-white transform translate-x-1/2" fill="currentColor" viewBox="0 0 100 100" preserveAspectRatio="none">
              <polygon points="50,0 100,0 50,100 0,100" />
            </svg>

            <main className="pt-10 mx-auto max-w-screen-xl px-4 sm:pt-12 sm:px-6 md:pt-16 lg:pt-20 lg:px-8 xl:pt-28">
              <div className="sm:text-center lg:text-left">

                <h1 className="text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:text-5xl sm:leading-none md:text-6xl">
                  {props.blok.headline}
                </h1>

                <div className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0" dangerouslySetInnerHTML={intro} />
                <div className="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
                  <div className="rounded-md shadow">
                    {props.blok.link.cached_url && <Link className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo transition duration-150 ease-in-out md:py-4 md:text-lg md:px-10" to={'/' + props.blok.link.cached_url}>
                      {props.blok.link_text}
                    </Link>}
                  </div>
                </div>

              </div>
            </main>
          </div>
        </div>
        {props.blok.image && props.blok.image.filename &&
          <div className="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
            <Image
              className="h-56 w-full object-cover sm:h-72 md:h-96 lg:w-full lg:h-full"
              image={props.blok.image}
              loading="auto"
              fadeIn={false}
              simpleFluid
            />
          </div>
        }
      </div>
    </SbEditable>
  )
};

export default Hero;
