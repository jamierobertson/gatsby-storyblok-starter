import React from 'react';
import SbEditable from 'storyblok-react';
import RichTextRender from '@utils/richTextRender';

const ContentBlock = props => {
  const rendered = RichTextRender(props.blok);

  return (
    <SbEditable content={props.blok}>
      <div className="py-12 bg-white">
        <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
          {props.blok.title && <h2 className="text-base leading-6 text-indigo-600 font-semibold tracking-wide uppercase">{props.blok.title}</h2>}
          {props.blok.subtitle && <h3 className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl sm:leading-10">{props.blok.subtitle}</h3>}
          {rendered && <div className="mt-4 leading-7 text-gray-500 lg:mx-auto" >{rendered}</div>}
        </div>
      </div>
    </SbEditable>
  )
};

export default ContentBlock;
