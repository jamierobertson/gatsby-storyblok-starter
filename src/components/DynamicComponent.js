
import React from 'react';

// Storyblok content types
import Post from '@contentTypes/post';
import Page from '@contentTypes/page';

// Components
import Placeholder from '@components/placeholder';
import Posts from '@components/posts/index';
import Card from '@components/card/index';
import Grid from '@components/grid/index';
import Feature from '@components/feature/index';
import ContentBlock from '@components/content_block';
import Hero from '@components/hero';
import NavItem from '@components/navigation/nav_item';
import ContactForm from '@components/contactForm';
import Image from '@components/image';
import Breadcrumbs from '@components/breadcrumb';
import FeaturedArticles from '@components/featuredArticles';

// Map local components to Storyblok technical names
const ComponentList = {
  grid: Grid,
  teaser: Hero,
  feature: Feature,
  nav_item: NavItem,
  post: Post,
  page: Page,
  post_list: Posts,
  card: Card,
  content_block: ContentBlock,
  contact_form: ContactForm,
  image_block: Image,
  breadcumb: Breadcrumbs,
  featured_articles: FeaturedArticles
}

const Components = ({ blok, ...otherProps }) => {
  if (typeof ComponentList[blok.component] !== 'undefined') {
    const Component = ComponentList[blok.component];
    return <Component blok={blok} {...otherProps} />
  }

  return <Placeholder componentName={blok.component} />
};

export default Components;
