import React from "react"

const DynamicPlaceholder = ({ componentName }) => (
  <div className="py-4 border border-green-200 bg-green-100">
    <p className="text-red-700 italic text-center">Dynamic area for <strong>{componentName}</strong>.</p>
  </div>
);

export default DynamicPlaceholder;