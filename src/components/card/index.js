import React from 'react';
import Link from 'gatsby-link';
import Image from '@components/image';

const Card = props => {
  const { content, full_slug } = props.blok;

  return (
    <div className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
      <Link to={`/${full_slug}`}>
        <div className="max-w-sm rounded overflow-hidden shadow-lg">
          {content.image && <Image className="w-full" image={content.image} imageOptions={{ width: 384, height: 256 }} simple />}
          <div className="px-6 py-4">
            <div className="font-bold text-xl mb-2">{content.title}</div>
            <p className="text-gray-700 text-base">{content.intro}</p>
          </div>
        </div>
      </Link>
    </div>
  )
};

export default Card;