import React from 'react';
import { render } from 'storyblok-rich-text-react-renderer';
import SbEditable from 'storyblok-react';
import Card from '@components/card';

const FeaturedArticles = ({ blok }) => {
  const content = (
    <SbEditable content={blok}>
      <h2 className="text-base leading-6 text-indigo-600 font-semibold tracking-wide uppercase">{blok.title}</h2>
      <div className="mt-4 text-xl leading-7 text-gray-500 lg:mx-auto">{render(blok.intro)}</div>
    </SbEditable>
  );

  const parsed = blok.featured.map((article, i) => {
    if (!article.content) return false;
    return <Card blok={article} key={i} />;
  })

  return (
    <div className="py-12 bg-white">
      <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8 ">
        {content}
        <div className="flex flex-wrap -mx-1 lg:-mx-4">
          {parsed && parsed}
        </div>
      </div>
    </div>
  );
}

export default FeaturedArticles;