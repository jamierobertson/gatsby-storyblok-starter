import React from 'react';
import SbEditable from 'storyblok-react';
import DynamicComponent from '@components/DynamicComponent';

const Grid = ({ blok }) => (
  <SbEditable content={blok} key={blok._uid}>
    <ul>
      {blok.columns.map((blok) => (
        <li key={blok._uid}>
          <DynamicComponent blok={blok} />
        </li>
      )
      )}
    </ul>
  </SbEditable>
);

export default Grid;
