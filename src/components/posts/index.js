import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import Link from 'gatsby-link';
import { render } from 'storyblok-rich-text-react-renderer';
import SbEditable from 'storyblok-react';
import Image from '@components/image';

// listing areas cant be accessed unless direct relations are set
// query must be done in component
const Posts = ({ blok }) => {
  const postQuery = useStaticQuery(graphql`
    {
      allStoryblokEntry(
        filter: {
          full_slug: { regex: "/blog/" }
          is_startpage: { eq: false }   
        }
        sort: {fields: first_published_at}
      ) {
        nodes {
          uuid
          full_slug
          first_published_at
          content
          is_startpage
          field_component
        }
      }
    }
  `);

  const posts = blok.nodes ? { nodes: [...blok.nodes] } : postQuery.allStoryblokEntry;

  const content = (
    <SbEditable content={blok}>
      <h2 className="text-base leading-6 text-indigo-600 font-semibold tracking-wide uppercase">{blok.title}</h2>
      <div className="mt-4 text-xl leading-7 text-gray-500 lg:mx-auto">{render(blok.intro)}</div>
    </SbEditable>
  );

  const parsed = posts.nodes.map(blok => {
    const post = JSON.parse(blok.content);

    return (
      <div className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3" key={blok.uuid}>
        <Link to={`/${blok.full_slug}`}>
          <div className="max-w-sm rounded overflow-hidden shadow-lg">
            {post.image && <Image className="w-full" image={post.image} imageOptions={{ width: 384, height: 256 }} simple />}
            <div className="px-6 py-4">
              {post.title && <div className="font-bold text-xl mb-2">{[post.title]}</div>}
              {post.intro && <p className="text-gray-700 text-base">{post.intro}</p>}
            </div>
          </div>
        </Link>
      </div>
    )
  });

  // Return our posts
  return (
    <div className="py-12 bg-white">
      <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8 ">
        {content}
        <div className="flex flex-wrap -mx-1 lg:-mx-4">
          {parsed}
        </div>
      </div>
    </div>
  );

  // @TODO: Add pagination example
}


export default Posts;
