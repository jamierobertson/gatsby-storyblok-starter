import React from 'react';
import SbEditable from 'storyblok-react';

const Feature = props => {
  return (
    <SbEditable content={props.blok}>
      <div className="col-4">
        <h2>{props.blok.name}</h2>
        {props.blok.image && <img src={props.blok.image.filename} className="w-100" alt="" />}
      </div>
    </SbEditable>
  )
};

export default Feature;
