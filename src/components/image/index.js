import React from 'react';
import Img from 'gatsby-image';
import { fixedImage, fluidImage } from '@utils/gatsbyImageTransform';

const Image = props => {
  const { blok } = props;
  let ImageProps;
  let ImageOptions;

  //  We just want the image tag
  if (props.simple && props.image) {
    ImageOptions = props.imageOptions || {};
    ImageProps = fixedImage(props.image.filename, ImageOptions);
    return <Img fixed={ImageProps} {...props} />;
  }

  if (props.simpleFluid && props.image) {
    ImageOptions = props.imageOptions || {};
    ImageProps = fluidImage(props.image.filename, ImageOptions);
    return <Img fluid={ImageProps} {...props} />;
  }


  // We want the wrappers, i.e. within the content block;
  ImageProps = fluidImage(blok.source.filename);

  const wrapperStyles = {
    display: 'flex',
    alignItems: blok.alignment,
    justifyContent: blok.alignment,
    marginBottom: '30px'
  }

  const innerStyles = {
    width: blok.width ? blok.width : 'auto'
  }

  return (
    <div style={wrapperStyles}>
      <div style={innerStyles}>
        <Img src={blok.source.filename} alt={blok.title} width={blok.width ? blok.width : 'auto'} fluid={ImageProps} style={{ 'flex': '1 0 auto' }} />
      </div>
    </div>
  )
}

export default Image;
