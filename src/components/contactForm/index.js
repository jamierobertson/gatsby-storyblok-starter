import React from 'react';
import { Formik, Form, Field } from 'formik';

const ContactForm = props => {
  return (
    <Formik
      initialValues={{
        name: '',
        email: '',
        message: '',
      }}
      onSubmit={(values, actions) => {
        console.log(JSON.stringify(values, null, 2));
        actions.setSubmitting(false);
      }}
      validate={values => {
        const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
        const errors = {};
        if (!values.name) {
          errors.name = 'Name Required'
        }
        if (!values.email || !emailRegex.test(values.email)) {
          errors.email = 'Valid Email Required'
        }
        if (!values.message) {
          errors.message = 'Message Required'
        }
        return errors;
      }}
    >
      {({ errors, touched, isSubmitting }) => (
        <Form id="contact-form" className="w-full mx-left bg-white p-8 pt-0 text-gray-700" noValidate>

          <div className="flex flex-wrap mb-6">
            <div className="relative w-full appearance-none label-floating">
              <Field className="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-gray-200 rounded focus:outline-none focus:bg-gray-100" id="name" type="text" name="name" placeholder="Your name" />
              {errors.name && touched.name ? <div className="bg-red-300 border-red-500 text-red-800 py-2 px-4 mb-3 rounded">{errors.name}</div> : null}
              <label
                htmlFor="name"
                className="absolute tracking-wide py-2 px-4 mb-4 opacity-0 leading-tight block top-0 left-0 cursor-text">
                Your name
                </label>
            </div>
          </div>


          <div className="flex flex-wrap mb-6">
            <div className="relative w-full appearance-none label-floating">
              <Field className="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-gray-200 rounded focus:outline-none focus:bg-gray-100" id="email" name="email" type="email" placeholder="Your email*" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required />
              {errors.email && touched.email ? <div className="bg-red-300 border-red-500 text-red-800 py-2 px-4 mb-3 rounded">{errors.email}</div> : null}
              <label
                htmlFor="email"
                className="absolute tracking-wide py-2 px-4 mb-4 opacity-0 leading-tight block top-0 left-0 cursor-text">
                Your email
                </label>
            </div>
          </div>


          <div className="flex flex-wrap mb-6">
            <div className="relative w-full appearance-none label-floating">
              <Field
                className="tracking-wide py-2 px-4 mb-3 leading-relaxed  appearance-none block w-full h-56 bg-gray-200 rounded focus:outline-none focus:bg-gray-100 resize-y"
                id="message"
                name="message"
                placeholder="Message..."
                component="textarea" />
              {errors.message && touched.message ? <div className="bg-red-300 border-red-500 text-red-800 py-2 px-4 mb-3 rounded">{errors.message}</div> : null}
              <label
                htmlFor="message"
                className="absolute tracking-wide pt-2 px-4 mb-4 opacity-0 leading-tight block top-0 left-0 cursor-text rounded">
                Message...
                </label>
            </div>
          </div>
          <button
            className="mt-4 relative sm:mt-0 sm:h-auto sm:ml-4 block w-full sm:w-auto border border-transparent px-6 py-3 text-base leading-6 font-semibold leading-snug bg-gray-900 text-white rounded-md shadow-md hover:bg-gray-800 focus:outline-none focus:bg-gray-800 transition ease-in-out duration-150 hover:bg-gray-600"
            type="submit"
            disabled={isSubmitting}>
            Send
            </button>
        </Form>
      )
      }
    </Formik>
  )
}

export default ContactForm;
