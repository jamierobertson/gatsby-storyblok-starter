import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import Navigation from '@components/navigation';
import Breadcrumbs from '@components/breadcrumb';
import Footer from '@components/footer';
import "../../css/index.css";


const Layout = ({ children, ...rest }) => {
  const { settings } = useStaticQuery(graphql`
    query Settings {
      settings: allStoryblokEntry(filter: {field_component: {eq: "settings"}}) {
        edges {
          node {
            name
            full_slug
            content
          }
        }
      }
    } 
  `);

  let isLanguage = '';
  let correctSetting = settings.edges.filter(edge => edge.node.full_slug.indexOf(isLanguage) > -1);
  let hasSetting = correctSetting && correctSetting.length ? correctSetting[0].node : {};
  let content = typeof hasSetting.content === 'string' ? JSON.parse(hasSetting.content) : hasSetting.content;

  const parsedSetting = Object.assign({}, content, { content: content });

  return (
    <div className="container mx-auto">
      <Navigation settings={parsedSetting} />
      <Breadcrumbs />
      <main>{children}</main>
      <Footer />
    </div>
  )
};

export default Layout;

