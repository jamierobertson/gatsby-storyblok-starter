import { getFixedGatsbyImage, getFluidGatsbyImage } from 'gatsby-storyblok-image';

const fluidImage = (image, params) => {
  return getFluidGatsbyImage(image, { ...params });
};

const fixedImage = (image, params) => {
  return getFixedGatsbyImage(image, { ...params });
};

export { fluidImage, fixedImage };
