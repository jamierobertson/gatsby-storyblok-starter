
import StoryblokClient from 'storyblok-js-client';

let Storyblok = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN
});

const CreateMarkup = (storyblokHTML) => {
  return {
    __html: Storyblok.richTextResolver.render(storyblokHTML),
  }
};

export default CreateMarkup;
