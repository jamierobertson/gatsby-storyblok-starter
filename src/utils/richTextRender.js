import React from 'react';
import { render } from 'storyblok-rich-text-react-renderer';
import Card from '@components/card';
import Image from '@components/image';

const RichTextRender = (props) => {

  if (!props) return false;

  // Add components you want to render inside a WYSOGWYG here
  const content = render(props.copy, {
    blokResolvers: {
      'card': props => <Card blok={props} />,
      'image_block': props => <Image blok={props} />
    }
  });

  return content;
}

export default RichTextRender;
