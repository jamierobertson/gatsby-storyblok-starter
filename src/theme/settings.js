const settings = {
  color: {
    primary: "#e63946",
    secondary: "#f1faee",
    tertiary: "#a8dadc",
    quaternary: "#457b9d",
    quinary: "#1d3557",
    text: "#161a1d",
    text_alt: "#FFFFFF",
    footer: "#1d3557",
    default: "#FFFFFF",
    link: {
      default: "#457b9d",
      hover: "#1d3557",
    },
  },
  layout: {
    inner: {
      maxWidth: "1160px",
      padding: {
        sml: "30px",
        md: "60px",
        lrg: "140px",
      },
    },
  },
  font: {
    heading: "Solway, serif",
    label: "Solway, serif",
    body: "Roboto, sans-serif",
    prefix: {
      sml: "20px",
    },
    heading: {
      sml: "34px",
    },
  },
  transitions: {
    slide: "cubic-bezier(0.4, 0.0, 0.2, 1)",
  }
}

export default settings;