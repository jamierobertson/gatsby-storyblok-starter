import GlobalStyle from "./global";
import settings from "./settings";
import media from "./media";
import breakpoints from "./breakpoint";
import animation from './animation';
import { Container, Inner } from "./layout";

const theme = { GlobalStyle, settings, media, breakpoints, animation, Container, Inner };

//export seperately for individual use
export { GlobalStyle, settings, breakpoints, media, animation, Container, Inner };

// full theme export as the default
export default theme;  