import React from 'react';
import Layout from '@components/layout';
import { graphql } from 'gatsby';
import StoryblokService from '../utils/storyblokService';
import DynamicComponent from '@components/DynamicComponent';
import SEO from '@components/seo';

export const query = graphql`
  {
    story: storyblokEntry(full_slug: { eq: "home" }) {
      name
      content
      full_slug
      uuid
      tag_list
    }
  }
`;

export default class extends React.Component {
  state = {
    story: {
      content: JSON.parse(this.props.data.story.content)
    }
  }

  async getInitialStory() {
    StoryblokService.setQuery(this.props.location.search)
    let { data: { story } } = await StoryblokService.get(`cdn/stories/${this.props.data.story.full_slug}`)
    return story;
  }

  async componentDidMount() {
    let story = await this.getInitialStory(this.props.params['*']);

    if (story.content) this.setState({ story });
    setTimeout(() => StoryblokService.initEditor(this), 200);
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" description="description of homepage..." />
        <DynamicComponent blok={this.state.story.content} />
      </Layout>
    )
  }
};
