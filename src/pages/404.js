import React from 'react';
import Layout from '@components/layout';
import Link from 'gatsby-link';

const ErrorPage = props => {
  return (
    <Layout>
      <div className="py-12 bg-white">
        <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
          <h2 className="text-base leading-6 text-indigo-600 font-semibold tracking-wide uppercase">Page not found</h2>
          <div className="mt-4 leading-7 text-gray-500 lg:mx-auto" >Please return to the <Link to="/">Homepage</Link>.</div>
        </div>
      </div>
    </Layout>
  )
};

export default ErrorPage;
