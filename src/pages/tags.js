import React from 'react';
import { Link, graphql } from 'gatsby';
import Layout from '@components/layout';

const TagsPage = ({ data }) => {
  const allTags = data.tags.group;

  return (
    <Layout>
      <div className="py-12 bg-white">
        <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1>Tags</h1>
          <ul>
            {allTags.map(tag => (
              <li key={tag.fieldValue}>
                <Link to={`/tags/${tag.fieldValue.toLowerCase()}/`}>
                  <span className="text-xs inline-flex items-center font-bold leading-sm uppercase px-3 py-1 mr-3 bg-blue-200 text-blue-700 rounded-full" key={tag}>
                    {tag.fieldValue} ({tag.totalCount})
                  </span>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </Layout>
  );
};

export default TagsPage;

export const pageQuery = graphql`
  query {
    tags: allStoryblokEntry {
      group(field: tag_list) {
        fieldValue
        totalCount
      }
      totalCount
    }
  }
`;