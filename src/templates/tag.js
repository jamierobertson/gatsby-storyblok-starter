import React from 'react';
import { graphql } from 'gatsby';
import Link from 'gatsby-link';
import Layout from '@components/layout';
import Posts from '@components/posts';
import SEO from '@components/seo';

const Tags = ({ pageContext, data }) => {
  const { tag } = pageContext;
  const { group } = data.tags;
  const tagGroup = group.filter(datum => datum.fieldValue === tag);

  return (
    <Layout>
      <SEO title={`Posts tagged ${tag}`} />
      <div className="py-12 pb-0 bg-white">
        <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1 className="text-xl leading-12 text-indigo-600 font-semibold tracking-wide uppercase">Posts tagged with "{tag}"</h1>
        </div>
      </div>
      <Posts blok={tagGroup[0]} />

      <div className="bg-white px-6 pt-2 pb-6 my-3 w-full mx-auto max-w-screen-xl flex items-center">
        <Link to="/tags" className="border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline">
          View all tags
      </Link>
      </div>
    </Layout>
  )
}

export default Tags;

export const pageQuery = graphql`
  query($tag: String) {
    tags: allStoryblokEntry(filter: {tag_list: {in: [$tag]}}) {
      group(field: tag_list) {
      fieldValue
      nodes {
          field_title_string
          full_slug
          content
        }
      }
    }

  }
`