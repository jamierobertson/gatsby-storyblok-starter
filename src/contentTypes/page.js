import React from "react";
import DynamicComponent from '@components/DynamicComponent';

// Page type defined in Storyblok
// Body is of field type "blocks"
const Page = ({ blok }) => {
  return (
    <>
      {blok.body && blok.body.map(blok => <DynamicComponent blok={blok} key={blok._uid} />)}
    </>
  );
};

export default Page;
