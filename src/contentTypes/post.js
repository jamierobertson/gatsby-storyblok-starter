import React from "react"
import SbEditable from "storyblok-react"
import DynamicComponent from '@components/DynamicComponent'
import AuthorCard from '@components/authorCard';
import TagList from '@components/taglist';

// Post type defined in Storyblok
const Post = ({ blok, tags }) => {
  return (
    <SbEditable content={blok} key={blok._uid}>
      <div className="bg-white">
        <div className="max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1 className="text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:text-5xl sm:leading-none md:text-5xl mb-0">{blok.title}</h1>
        </div>
      </div>
      {blok.body && blok.body.map(blok => <DynamicComponent blok={blok} key={blok._uid} />)}

      {/* Bolt on our Tags */}
      {tags && <TagList {...tags} />}

      {/* Bolt on on the Author */}
      {blok.author && <AuthorCard blok={blok.author} />}
    </SbEditable>
  )
};

export default Post;

