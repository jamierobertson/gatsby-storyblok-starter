## Creating a new seed file
- Install storyblok cli
- Login using `storyblok login` and entering your credentials
- Run from within this folder, `storyblok pull-components --space={{SPACE_ID_GOES_HERE}}`.
- Rename file or copy contents into `components.seed.json`. Replace

