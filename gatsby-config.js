
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: `Stroyblok Gatsby Starter`,
    titleTemplate: "%s · Gatsby storybook starter",
    description: `Storyblok / Gatsby starting point with live preview`,
    author: `@theleith`,
    siteUrl: 'http://www.example.com',
    image: '',
    twitterUsername: ''
  },
  plugins: [
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {
          '@theme': 'src/theme',
          '@components': 'src/components',
          '@contentTypes': 'src/contentTypes',
          '@utils': 'src/utils'
        },
        extensions: [
          "js",
        ],
      }
    },
    {
      resolve: 'gatsby-source-storyblok',
      options: {
        accessToken: process.env.GATSBY_STORYBLOK_ACCESS_TOKEN,
        homeSlug: 'home',
        version: process.env.NODE_ENV === 'production' ? 'published' : 'draft',
        resolveRelations: ['featured_articles.featured', 'post.author'],
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    // {
    //   resolve: 'gatsby-plugin-robots-txt',
    //   options: {
    //     host: 'https://www.example.com',
    //     sitemap: 'https://www.example.com/sitemap.xml',
    //     resolveEnv: () => process.env.GATSBY_ENV,
    //     env: {
    //       development: {
    //         policy: [{ userAgent: '*', disallow: ['/'] }]
    //       },
    //       production: {
    //         policy: [{ userAgent: '*', allow: '/' }]
    //       }
    //     }
    //   }
    // },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-storyblok-starter`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-postcss`,
    // {
    //   resolve: `gatsby-plugin-google-fonts`,
    //   options: {
    //     fonts: [
    //       `Roboto`,
    //     ],
    //     display: "swap",
    //   },
    // },
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/preview/*`] },
    },
    `gatsby-alias-imports`,
    `gatsby-plugin-accessibilityjs`,
    {
      resolve: 'gatsby-plugin-bundle-stats',
      options: {
        compare: true,
        outDir: '../artifacts',
        stats: {
          context: './src'
        }
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-external-links",
            options: {
              target: "_self",
              rel: "nofollow"
            }
          }
        ]
      }
    },
    {
      resolve: 'gatsby-plugin-html-attributes',
      options: {
        lang: 'en'
      }
    },
    // {
    //   resolve: `gatsby-plugin-gdpr-cookies`,
    //   options: {
    //     googleAnalytics: {
    //       trackingId: 'YOUR_GOOGLE_ANALYTICS_TRACKING_ID', // leave empty if you want to disable the tracker
    //       cookieName: 'gatsby-gdpr-google-analytics', // default
    //       anonymize: true, // default
    //       allowAdFeatures: false // default
    //     },
    //     googleTagManager: {
    //       trackingId: 'YOUR_GOOGLE_TAG_MANAGER_TRACKING_ID', // leave empty if you want to disable the tracker
    //       cookieName: 'gatsby-gdpr-google-tagmanager', // default
    //       dataLayerName: 'dataLayer', // default
    //     },
    //     facebookPixel: {
    //       pixelId: 'YOUR_FACEBOOK_PIXEL_ID', // leave empty if you want to disable the tracker
    //       cookieName: 'gatsby-gdpr-facebook-pixel', // default
    //     },
    //     // defines the environments where the tracking should be available  - default is ["production"]
    //     environments: ['production', 'development']
    //   },
    // },
    `gatsby-plugin-remove-serviceworker`
  ]
}
