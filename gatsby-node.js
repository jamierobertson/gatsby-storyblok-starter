const path = require("path");

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    node: {
      fs: 'empty'
    }
  });
};

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    // Files used here live in /src/templates
    const Story = path.resolve('src/templates/story.js');
    const Tag = path.resolve('src/templates/tag.js')

    resolve(
      graphql(
        `{
          stories: allStoryblokEntry {
            edges {
              node {
                id
                name
                slug
                field_component
                full_slug
                content
                tag_list
              }
            }
          }
          tags: allStoryblokEntry {
            group(field: tag_list) {
              fieldValue
              edges {
                node {
                  slug
                }
              }
            }
          }
        }`
      ).then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        const entries = result.data.stories.edges
        const tags = result.data.tags.group;

        // Build all our standard stories/pages
        entries.forEach(entry => {
          const page = {
            path: `/${entry.node.full_slug}`,
            component: Story,
            context: {
              story: entry.node
            }
          }
          createPage(page)
        });

        // Build our tag pages
        tags.forEach(tag => {
          const page = {
            path: `/tags/${tag.fieldValue}/`,
            component: Tag,
            context: {
              tag: tag.fieldValue,
            },
          };

          createPage(page);
        });
      })
    )
  })
}